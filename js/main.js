var dssv = [];

var dataJson = localStorage.getItem("DSSV")
if (dataJson != null)
{
    dssv = JSON.parse(dataJson).map(function(item){
        return new SinhVien(item.ma,item.ten,item.email,item.matKhau,item.toan,item.ly,item.hoa);
    });
    //map
    render(dssv);
}

function themSinhVien(){
    var sv = layThongTinTuForm();
    dssv.push(sv);
    //in danh sách lên giao diện
    render(dssv);
    //lưu trữ trong dataStorage
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dataJson);
}

function Reset(){
    document.getElementById("formQLSV").reset();
}

function xoaSinhVien(id){
    var index = dssv.findIndex(function(item){
        return item.ma = id; 
    })
    dssv.splice(index,1);
    render(dssv);
}

function suaSinhVien(id){
    var index = dssv.findIndex(function (item){
        return item.ma == id;
    });
    //show thong tin len form
    showThongTinLenForm(dssv[index]);
}

function Update(){
    var sv = layThongTinTuForm();
    for(var i = 0; i < dssv.length;i++)
    {
        if(dssv[i].ma == sv.ma)
        {
            dssv[i].ten = sv.ten;
            dssv[i].email = sv.email;
            dssv[i].matKhau = sv.matKhau;
            dssv[i].toan = sv.toan;
            dssv[i].ly = sv.ly;
            dssv[i].hoa = sv.hoa;
            //DOM hiển thị dssv
            document.getElementById("txtTenSV").innerHTML = dssv[i].ten;
            document.getElementById("txtEmail").innerHTML = dssv[i].email;
            document.getElementById("txtPass").innerHTML = dssv[i].matKhau;
            document.getElementById("txtDiemToan").innerHTML = dssv[i].toan;
            document.getElementById("txtDiemLy").innerHTML = dssv[i].ly;
            document.getElementById("txtDiemHoa").innerHTML = dssv[i].hoa;
        }
    }
    render(dssv);
    //lưu trữ trong dataStorage
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dataJson);
}
